/** Javascipt file.
 * 	Lets keep this reasonable.
 * 	We don't need anything fancy fancy.
 *  ... Thanks.
 */

var startUp {

/**
 * Listens for incoming RPC calls from the browser action and content scripts
 * and takes the appropriate actions.
 * @private
 *
 * P.S. This is actually Googles. Lol
 */
background.listenForRequests_ = function() {
  chrome.extension.onMessage.addListener(function(request, sender, opt_callback) {
    switch(request.method) {
      case 'events.detected.set':
        background.selectedTabId = sender.tab.id;
        background.eventsFromPage['tab' + background.selectedTabId] = request.parameters.events;
        chrome.browserAction.setIcon({
          path: 'icons/calendar_add_19.png',
          tabId: sender.tab.id
        });
        break;

      case 'events.detected.get':
        if (opt_callback) {
          opt_callback(background.eventsFromPage['tab' + background.selectedTabId]);
        }
        break;

      case 'events.feed.get':
        if (opt_callback) {
          opt_callback(feeds.events);
        }
        break;

      case 'events.feed.fetch':
        feeds.fetchCalendars();
        break;

      case 'options.changed':
        feeds.refreshUI();
        break;
    }

    // Indicates to Chrome that a pending async request will eventually issue
    // the callback passed to this function.
    return true;
  });
};

 requestCalendar: function () {
 	// TODO: Get the calendar from the website and display
 }


}